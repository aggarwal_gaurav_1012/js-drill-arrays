function flatten(elements) {

    const flattenedArray = [];

    for (let index = 0; index < elements.length; index++) {
        if (Array.isArray(elements[index])) {
            // If the current element is an array, the 'flatten' function is called recursively
            flattenedArray.push(...flatten(elements[index]));
        } else {
            flattenedArray.push(elements[index]);
        }
    }
    return flattenedArray;
}

module.exports = flatten;