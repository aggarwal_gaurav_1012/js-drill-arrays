function reduce(elements, cb, startingValue) {

    // Determining the initial value of the accumulator and the starting index for the iteration
    let accumulator = startingValue !== undefined ? startingValue : elements[0];
    const startIdx = startingValue !== undefined ? 0 : 1;

    for (let index = startIdx; index < elements.length; index++) {
        accumulator = cb(accumulator, elements[index]);
    }
    return accumulator;
}

module.exports = reduce;