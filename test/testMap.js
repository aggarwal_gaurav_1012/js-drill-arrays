const map = require('../functions/map');

// Array for testing
const items = [1, 2, 3, 4, 5, 5];

// Test map function with a simple callback
const mappedArray = map(items, (element) => {
    return element * 2;
});

console.log(mappedArray);