const flatten = require('../functions/flatten');

// Array for testing
const nestedArray = [1, [2], [[3]], [[[4]]], [[[[5]]]], [[[[[5]]]]]];

// Test flatten function
const flattenedArray = flatten(nestedArray);
console.log(flattenedArray);