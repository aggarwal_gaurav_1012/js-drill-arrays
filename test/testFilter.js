const filter = require('../functions/filter');

// Array for testing
const items = [1, 2, 3, 4, 5, 5];

// Test filter function with a simple callback
const filteredArray = filter(items, (element) => {
    return element > 2;
});

console.log(filteredArray);