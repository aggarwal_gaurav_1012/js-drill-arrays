const find = require('../functions/find');

// Array for testing
const items = [1, 2, 3, 4, 5, 5];

// Test find function with a simple callback
const foundItem = find(items, (element) => {
    return element === 4;
});

console.log(foundItem);