const each = require('../functions/each');

// Array for testing
const items = [1, 2, 3, 4, 5, 5];

// Test each function with a simple callback
each(items, (element, index) => {
    console.log(`Index ${index}: ${element}`);
});