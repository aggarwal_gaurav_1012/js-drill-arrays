const reduce = require('../functions/reduce');

// Array for testing
const items = [1, 2, 3, 4, 5, 5];

// Test reduce function with a simple callback
const sum = reduce(items, (accumulator, currentValue) => {
    return accumulator + currentValue;
}, 0);

console.log(sum);